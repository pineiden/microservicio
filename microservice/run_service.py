import asyncio
import concurrent.futures
import socket
from networktools.colorprint import gprint, bprint, rprint
from microservice.socket_server import GNCSocketServer
from microservice.service import MicroService
from multiprocessing import Manager


if __name__ == "__main__":
    address = (socket.gethostbyname(socket.gethostname()), 5500)
    workers = 2
    with concurrent.futures.ProcessPoolExecutor(workers) as executor:
        manager = Manager()
        loop = asyncio.get_event_loop()
        queue_n2t = manager.Queue()
        queue_t2n = manager.Queue()
        server = GNCSocketServer(queue_t2n, queue_n2t, address=address)
        scheduler_service = MicroService(queue_t2n, queue_n2t)
        loop.run_in_executor(executor, server.socket_task)
        loop.run_in_executor(executor, scheduler_service.run_task)
        loop.run_forever()
