import socket
import functools
import asyncio
from microservice.funciones import (sumar, multiplicar, mayusculas, minusculas)
from tasktools.taskloop import coromask, renew, simple_fargs, simple_fargs_out
from tasktools.scheduler import TaskScheduler
from networktools.colorprint import gprint, bprint, rprint
from networktools.library import my_random_string
from networktools.queue import read_queue_gen


class MicroService:
    def __init__(self, qt2n, qn2t, *args, **kwargs):
        self.qt2n = qt2n
        self.qn2t = qn2t
        super().__init__(*args, **kwargs)
        self.opts = {
            'sumar': sumar,
            'multiplicar': multiplicar,
            'mayusculas': mayusculas,
            'minusculas': minusculas
        }
        self.clients = {}

    async def process_data(self, *args, **kwargs):
        queue_in = self.qn2t
        queue_out = self.qt2n
        for msg in read_queue_gen(queue_in):
            idm = msg.get('idm')
            command = msg.get('command')
            args = msg.get('args', [])
            kwargs = msg.get('kwargs', {})
            bprint("WM"*20)
            print(command)
            print(args, type(args))
            fn = self.opts.get(command, print)
            print("Funcion->", fn)
            bprint("WM"*20)
            result = fn(*args, **kwargs)
            rprint("Result->%s" % result)
            result_dict = {'idm': idm, 'answer': result}
            gprint("°|°"*10)
            rprint(result_dict)
            gprint("°|°"*10)
            queue_out.put(result_dict)

    def run_task(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        tasks = []
        try:
            args = []
            #bprint("Args to sta manager %s" %args)
            task = loop.create_task(
                coromask(
                    self.process_data,
                    args,
                    simple_fargs)
            )
            task.add_done_callback(
                functools.partial(renew,
                                  task,
                                  self.process_data,
                                  simple_fargs)
            )
            tasks.append(task)
        except Exception as exec:
            print(exec)
            raise exec
        if not loop.is_running():
            loop.run_forever()
