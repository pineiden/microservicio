from PySide2.QtWidgets import QApplication
from quamash import QEventLoop, QThreadExecutor
import sys
import queue
import asyncio
import csv
import ipaddress
import functools as fc
import ujson as json
from PySide2.QtWidgets import QMainWindow
from PySide2.QtWidgets import QWidget
from PySide2.QtWidgets import QVBoxLayout, QHBoxLayout
from PySide2.QtWidgets import QPushButton
from PySide2.QtWidgets import QDialog
from PySide2.QtWidgets import QLineEdit, QLabel, QComboBox, QTextEdit
from PySide2.QtCore import QModelIndex
from collections import OrderedDict
from PySide2.QtCore import QTimer
from datetime import datetime, timedelta
from networktools.library import my_random_string
from PySide2.QtWidgets import QTableView
from PySide2.QtCore import Qt
from networktools.library import check_type
import re
from networktools.colorprint import gprint, bprint, rprint

from networktools.queue import send_queue, read_queue_gen
from auth_gui.tools import read_css, read_queue, center_window, print_dict


class ClientMicroServiceGUI(QMainWindow):
    opts = {
        'command': '',
        'args': [],
        'kwargs': {},
        'idm': ''
    }
    main_widgets = {}
    sections_layout = {}
    action_buttons = {}

    def __init__(self,
                 qg2s,
                 qs2g,
                 parent=None, *args, **kwargs):
        super().__init__(parent)
        self.qg2s = qg2s
        self.qs2g = qs2g
        self.setWindowTitle("Gestion de Usuarios y Servicios GNSS")
        self._status_update_timer = None
        self.setGeometry(0, 0, 1200, 800)
        self.user_data = kwargs.get('user_data', {})
        self.token = kwargs.get('token', {})
        # center_window(self)
        print("Building gui")
        self.actions = ['sumar', 'multiplicar', 'mayusculas', 'minusculas']
        self.build_gui()

    def build_gui(self):
        main_widget = QWidget()
        self.setCentralWidget(main_widget)
        self.layout = QVBoxLayout(main_widget)
        geometry = self.layout.contentsRect()
        self.main_widgets.update({'central': main_widget})
        self.sections_layout.update({'main': self.layout})
        command_widget = self.show_commands()
        self.main_widgets.update({'command': command_widget})
        self.layout.addWidget(command_widget)
        args_widget = self.show_content()
        self.layout.addWidget(args_widget)
        self.main_widgets.update({'args': args_widget})
        result_widget = self.show_result()
        self.layout.addWidget(result_widget)
        self.main_widgets.update({'result': result_widget})
        buttons_widget = self.show_buttons_actions()
        self.layout.addWidget(buttons_widget)
        self.main_widgets.update({'buttons': buttons_widget})

    def show_commands(self):
        widget = QComboBox()
        widget.insertItems(0, self.actions)
        widget.activated.connect(self.select_item)
        value = self.actions[0]
        self.init_select_item('command', value)
        return widget

    def init_select_item(self, key, value):
        self.opts.update({key: value})

    def select_item(self, value):
        command = self.actions[value]
        self.opts.update({'command': command})

    def show_content(self):
        widget = QLineEdit()
        widget.textChanged.connect(self.change_text)
        return widget

    def change_text(self, text):
        # check []
        self.opts.update({'args': text})

    def send_args_check(self):
        text = self.opts.get('args')
        pattern = re.compile("^\[.+\]")
        if not pattern.match(text):
            text = "[\"%s\"]" % text
            print("New texto->", text)
        try:
            self.opts.update({'args': json.loads(text)})
        except Exception as e:
            print("Error en args", e)
            raise e

    def show_result(self):
        widget = QTextEdit()
        return widget

    def show_buttons_actions(self):
        widget = QWidget()
        buttons = [
            QPushButton("Cancelar"),
            QPushButton("Enviar")]
        layout_actions = QHBoxLayout()
        [layout_actions.addWidget(button) for button in buttons]
        self.action_buttons.update(dict(
            zip(
                ('cancel', 'send'),
                buttons
            )
        )
        )
        cancel = self.action_buttons.get('cancel')
        cancel.clicked.connect(self.close)
        enviar = self.action_buttons.get('send')
        enviar.clicked.connect(self.send_action)
        widget.setLayout(layout_actions)
        return widget

    def send_action(self):
        self.opts.update({'idm': my_random_string()})
        msg = self.opts
        self.send_args_check()
        if self.qg2s:
            bprint("Send msg %s" % msg)
            self.qg2s.put(msg)
            self.set_qtimer_read_queue(
                self.qs2g, self.read_answer, time_value_ms=1000)
            # acivate read return

    def set_qtimer_read_queue(self,
                              queue_name,
                              sel_callback,
                              time_value_ms: int = 100) -> None:
        if not self._status_update_timer:
            self._status_update_timer = QTimer(self)
            self._status_update_timer.setSingleShot(False)
            self._status_update_timer.timeout.connect(
                fc.partial(read_queue, queue_name, callback=sel_callback))
            self._status_update_timer.start(time_value_ms)
        else:
            return self._status_update_timer.start()

    def read_answer(self, msg):
        print("Respuesta recibida_>", msg, type(msg))
        result_widget = self.main_widgets.get('result')
        result_widget.setText(json.dumps(msg))


AEventLoop = type(asyncio.get_event_loop())

# Una clase intermedia o bridge para juntar
# los eventloop async


class QEventLoopPlus(QEventLoop, AEventLoop):
    pass


def run_interfaz(kwargs):
    app = QApplication(sys.argv)
    loop = QEventLoopPlus(app)
    asyncio.set_event_loop(loop)
    qg2s = kwargs.get('queue_g2s')
    qs2g = kwargs.get('queue_s2g')
    ex = ClientMicroServiceGUI(qs2g, qg2s, **kwargs)
    ex.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    queue_x = queue.Queue()
    options = {
        'queue_g2s': queue.Queue(),
        'queue_s2g': queue.Queue(),
    }
    run_interfaz(options)
