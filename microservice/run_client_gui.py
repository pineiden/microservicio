from quamash import QEventLoop, QThreadExecutor
import sys
from PySide2.QtWidgets import QApplication
from multiprocessing import Manager
import queue
import signal
import concurrent.futures as cf
from microservice.client_gui import ClientMicroServiceGUI
import socket
from networktools.queue import read_queue_gen
from gnsocket.gn_socket import GNCSocket
# Standar lib
import asyncio
import functools
from multiprocessing import Manager, Queue, Lock
import time
# contrib modules
import ujson as json

# Own module
from gnsocket.gn_socket import GNCSocket

# module tasktools
from tasktools.taskloop import coromask, renew, simple_fargs
from networktools.colorprint import gprint, bprint, rprint

from networktools.library import pattern_value, \
    fill_pattern, context_split, \
    gns_loads, gns_dumps
from networktools.library import my_random_string

tsleep = 2


class GNCSocketClient:

    def __init__(self, qs2g, qg2s, *args, **kwargs):
        self.address = kwargs.get('address', ('localhost', 6666))
        self.qs2g = qs2g
        self.qg2s = qg2s

    async def sock_write(self, gs, idc):
        # Receive from sources and send data to clients
        await asyncio.sleep(tsleep)
        try:
            rprint("Cola gui to socket read-> %s <%s>" %
                   (self.qg2s, self.qg2s.empty()))
            for msg in read_queue_gen(self.qg2s):
                msg_send = json.dumps(msg)
                await gs.send_msg(msg_send, idc)
        except Exception as exec:
            print("Error con modulo de escritura del socket")
            raise exec

    async def sock_read(self, gs, idc):
        # Receive from sources and send data to clients
        await asyncio.sleep(tsleep)
        try:
            rprint("Reading sock on client -->")
            datagram = await gs.recv_msg(idc)
            bprint("Datagrama->%s" % datagram)
            if datagram not in {'', "<END>", 'null', None}:
                gprint("Enviando a cola")
                msg_dict = json.loads(datagram)
                print("Cola socket a gui->", self.qs2g)
                self.qs2g.put(msg_dict)
        except Exception as exec:
            print("Error con modulo de escritura del socket")
            raise exec

    def socket_task(self):
        # print("XDX socket loop inside", flush=True)
        loop = asyncio.get_event_loop()
        time.sleep(1)
        with GNCSocket(mode='client') as gs:
            self.loop = loop
            gs.set_address(self.address)
            gs.set_loop(loop)
            try:
                idc = loop.run_until_complete(gs.create_client())
                # First time welcome
                # task reader
                args = [gs, idc]
                task_1 = loop.create_task(
                    coromask(
                        self.sock_read,
                        args,
                        simple_fargs)
                )
                task_1.add_done_callback(
                    functools.partial(
                        renew,
                        task_1,
                        self.sock_read,
                        simple_fargs)
                )
                args = [gs, idc]
                # task write
                task_2 = loop.create_task(
                    coromask(
                        self.sock_write,
                        args,
                        simple_fargs)
                )
                task_2.add_done_callback(
                    functools.partial(
                        renew,
                        task_2,
                        self.sock_write,
                        simple_fargs)
                )
                tasks = [task_1, task_2]
                if not loop.is_running():
                    loop.run_forever()
                else:
                    loop.run_until_complete(asyncio.gather(*tasks))
            except KeyboardInterrupt:
                print("Closing socket on server")
                gs.close()
                print("Doing wait_closed")
                loop.run_until_complete(gs.wait_closed())
            except Exception as ex:
                print("Otra exception", ex)
            finally:
                print("Clossing loop on server")
                # loop.close()

        def close(self):
            self.gs.close()


def run_interfaz(qs2g, qg2s):
    print("RUNNING GUI============================")
    app = QApplication(sys.argv)
    loop = asyncio.get_event_loop()
    asyncio.set_event_loop(loop)
    q = {}
    ex = ClientMicroServiceGUI(qs2g, qg2s, **q)
    ex.show()
    sys.exit(app.exec_())
    if not loop.is_running():
        loop.run_forever()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    workers = 3
    executor = cf.ProcessPoolExecutor(workers)
    manager = Manager()
    qg2s = manager.Queue()
    qs2g = manager.Queue()
    address = (socket.gethostbyname(socket.gethostname()), 5500)
    client = GNCSocketClient(qs2g, qg2s, address=address)
    gui_task = loop.run_in_executor(
        executor, functools.partial(run_interfaz, qg2s, qs2g))
    socket_task = loop.run_in_executor(executor, client.socket_task)
    #tasks = [gui_task]
    tasks = [socket_task, gui_task]
    print("Check if loop", loop.is_running())
    if not loop.is_running():
        loop.run_until_complete(asyncio.gather(*tasks))
