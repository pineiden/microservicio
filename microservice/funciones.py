from functools import reduce
import operator 

def sumar(*args):
    print("Sumar->", args)
    suma = reduce(operator.add, args)
    print(suma)
    return suma

def multiplicar(*args):
    return reduce(operator.mul, args)

def mayusculas(text_list):
    result = []
    for text in text_list:
        if isinstance(text, str):
            result.append(text.upper())
        else:
            result.append(str(text).upper())
    return [''.join(result)]

def minusculas(text_list):
    result = []
    for text in text_list:
        if isinstance(text, str):
            result.append(text.lower())
        else:
            result.append(str(text).lower())
    return [''.join(result)]
