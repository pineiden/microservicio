import socket
from networktools.queue import read_queue_gen
from gnsocket.gn_socket import GNCSocket
# Standar lib
import asyncio
import functools
from multiprocessing import Manager, Queue, Lock

# contrib modules
import ujson as json

# Own module
from gnsocket.gn_socket import GNCSocket

# module tasktools
from tasktools.taskloop import coromask, renew, simple_fargs
from networktools.colorprint import gprint, bprint, rprint

from networktools.library import pattern_value, \
    fill_pattern, context_split, \
    gns_loads, gns_dumps
from networktools.library import my_random_string

tsleep = 2


class GNCSocketClient:

    def __init__(self, *args, **kwargs):
        print("Init Client->", kwargs)
        self.address = kwargs.get('address', ('localhost', 6666))

    async def sock_write(self, gs, idc):
        # Receive from sources and send data to clients
        await asyncio.sleep(tsleep)
        try:
            command = input(
                'Opciones : {sumar, multiplicar, mayusculas, minusculas}\n')
            args_str = input("Valores de entrada\n")
            args = json.loads(args_str)
            kwargs = {}
            idm = my_random_string()
            msg = dict(
                zip(('idm', 'command', 'args', 'kwargs'), (idm, command, args, kwargs)))
            msg_send = json.dumps(msg)
            await gs.send_msg(msg_send, idc)
        except Exception as exec:
            print("Error con modulo de escritura del socket")
            raise exec

    async def sock_read(self, gs, idc):
        # Receive from sources and send data to clients
        await asyncio.sleep(tsleep)
        try:
            rprint("Reading sock on client -->")
            datagram = await gs.recv_msg(idc)
            bprint(datagram)
            if datagram not in {'', "<END>", 'null', None}:
                msg_dict = json.loads(datagram)
                bprint("Result pre====")
                print(type(msg_dict), msg_dict)
                bprint("Result post====")
                [bprint("%s->%s" % (key, value))
                 for key, value in msg_dict.items()]
        except Exception as exec:
            print("Error con modulo de escritura del socket")
            raise exec

    def socket_task(self):
        # print("XDX socket loop inside", flush=True)
        loop = asyncio.get_event_loop()
        with GNCSocket(mode='client') as gs:
            self.loop = loop
            gs.set_address(self.address)
            gs.set_loop(loop)
            try:
                print("Pre create client")
                idc = loop.run_until_complete(gs.create_client())
                print("Post create client")
                # First time welcome
                # task reader
                args = [gs, idc]
                task_1 = loop.create_task(
                    coromask(
                        self.sock_read,
                        args,
                        simple_fargs)
                )
                task_1.add_done_callback(
                    functools.partial(
                        renew,
                        task_1,
                        self.sock_read,
                        simple_fargs)
                )
                args = [gs, idc]
                # task write
                task_2 = loop.create_task(
                    coromask(
                        self.sock_write,
                        args,
                        simple_fargs)
                )
                task_2.add_done_callback(
                    functools.partial(
                        renew,
                        task_2,
                        self.sock_write,
                        simple_fargs)
                )
                tasks = [task_1, task_2]
                if not loop.is_running():
                    loop.run_forever()
                else:
                    loop.run_until_complete(asyncio.gather(*tasks))
            except KeyboardInterrupt:
                print("Closing socket on server")
                gs.close()
                print("Doing wait_closed")
                loop.run_until_complete(gs.wait_closed())
            except Exception as ex:
                print("Otra exception", ex)
            finally:
                print("Clossing loop on server")
                # loop.close()

        def close(self):
            self.gs.close()


if __name__ == "__main__":
    address = (socket.gethostbyname(socket.gethostname()), 5500)
    client = GNCSocketClient(address=address)
    client.socket_task()
