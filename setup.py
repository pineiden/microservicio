from setuptools import setup
from networktools.requeriments import read

requeriments = read()

setup(name='MicroService',
      version='0.1',
      description='A service to test socket communication',
      url='',
      author='David Pineda Osorio',
      author_email='dpineda@uchile.cl',
      license='GPL',
      install_requires=requeriments,
      packages=[],
      zip_safe=False)
